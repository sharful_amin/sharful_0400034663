@extends('backend.layouts.master');

@section('content')

            <div class="container mt-5">
                <form action="{{route('product.update', $data->id)}}" method="POST">

                    @csrf

                    <div class="mb-3">
                      <label for="name" class="form-label">Product Name :</label>
                      <input 
                        type="text" 
                        class="form-control" 
                        id="name"
                        name="name"
                        value={{$data->name}}
                        placeholder="Please Enter Product Name"
                        >
                    </div>
                    <div class="mb-3">
                      <label for="name" class="form-label">Product price :</label>
                      <input 
                        type="number" 
                        class="form-control" 
                        id="price"
                        name="price"
                        placeholder="Please Enter Product price"
                        value={{$data->price}}
                        >

                    @error('price')
                           <span class="text-danger">{{$message}}</span>
                    @enderror


                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                  </form>
            </div>

@endsection