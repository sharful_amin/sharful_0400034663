@extends('backend.layouts.master');

@section('content')

            <div class="container mt-5">
                <form action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">

                    @csrf

                    <div class="mb-3">
                      <label for="name" class="form-label">Product Name :</label>
                      <input 
                        type="text" 
                        class="form-control" 
                        id="name"
                        name="name"
                        placeholder="Please Enter Product Name"
                        value="{{old('name')}}"
                        >

                    @error('name')
                           <span class="text-danger">{{$message}}</span>
                    @enderror
                    </div>
                    <div class="mb-3">
                      <label for="name" class="form-label">Product price :</label>
                      <input 
                        type="number" 
                        class="form-control" 
                        id="price"
                        name="price"
                        placeholder="Please Enter Product price"
                        value="{{old('price')}}"
                        >

                    @error('price')
                           <span class="text-danger">{{$message}}</span>
                    @enderror


                    </div>
                    <div class="mb-3">
                      <label for="image" class="form-label">Product Image :</label>
                      <input 
                        type="file" 
                        class="form-control" 
                        id="image"
                        name="image"
                        
                        {{-- value="{{old('image')}}" --}}
                        >

                    @error('image')
                           <span class="text-danger">{{$message}}</span>
                    @enderror


                    </div>

                    <div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>
            </div>

@endsection