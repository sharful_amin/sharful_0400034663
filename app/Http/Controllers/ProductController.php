<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use Exception;
// use OnlyTrashed;


class ProductController extends Controller
{

    public function index(){
        // return view('backend.products.index');

        // dd('in');

        $products = Product::all();
        // dd($data);
        return view('backend.products.index', compact('products'));
    }

    public function create(){
        // dd('ashche');
        return view('backend.products.create');
    }

    public function store(ProductRequest $request, Product $product){

        // $request->validate([
        //     'name'=>'required|unique:products'
        // ]);

        // dd($request);

        // dd('ashche');
        // dd($request->all());
        // dd($request->name);

        // Product::create([
        //     'name'=> $request->name,
        //     'price'=> $request->price,
           
        // ]);
        // $data=new Product();


        // return redirect() -> route('product.index');

        try{
            $products = $request -> all();
            Product::create($products);
            //  $data=new Product();
            //  if($request->file('image')){
            //     $file= $request->file('image');
            //     $filename= date('YmdHi').$file->getClientOriginalName();
            //     $file-> move(public_path('public/Image'),$filename);
            //     // $data['image']= $filename;
            // }


            $input = $request->all();
  
            if ($image = $request->file('image')) {
                $destinationPath = 'image/';
                $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $input['image'] = "$profileImage";
            }else{
                unset($input['image']);
            }
              
            $product->update($input);



            // $data->save();
            return redirect()->route('product.index')->withMessage('Product Added Successfully!');
        }
        catch(Exception $e){
            // dd($e -> getMessage());
            return redirect()->route('product.index')->withErrors($e->getMessage()); 
        }
    }

    public function edit($id){
        // dd("check");
        // dd($id);
        $data = Product::where('id', $id) -> first();
        // dd($data);
        return view('backend.products.edit', compact('data'));
    }

    public function update(Request $request, $id){
        try{
            // dd($request->all());
            // $data = $request->all();
            $data = $request->except('_token');

            Product::where('id', $id)->update($data);
            return redirect()->route('product.index')->withMessage('Product Updated Successfully Done !');
        }
        catch(Exception $e){
            return redirect()->route('product.index')->withErrors($e->getMessage());
        }
    }

    public function destroy($id){
        // dd($id);
        // Product::find($id)->delete();
        // return redirect()->route('product.index');

        // findorFail($id);

        $product = Product::find($id);
        $product->delete();
        return redirect()->route('product.index')->withMessage('Product is deleted Successfully !');
    }

    // trash methods

    // public function trashlist(){
    //     $data = Product::onlyTrashed()->get();
    //     return view('backend.products.trashlist', compact('data'));
    // }
}
